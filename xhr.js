window.onload = () => {
    const xhr = new XMLHttpRequest();

    xhr.open('POST', 'https://reqres.in/api/login');
    xhr.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
    xhr.send(JSON.stringify({
        "username": "george.bluth@reqres.in",
        "email": "george.bluth@reqres.in",
        "password": "george"
    }));

    xhr.onloadend = () => {
        const token = xhr.response;
        const xhrUsers = new XMLHttpRequest();
        xhrUsers.open('GET', 'https://reqres.in/api/users?per_page=10');

        xhrUsers.setRequestHeader('Cache-Control', 'no-cache');
        xhrUsers.setRequestHeader('Content-Type', 'application/json;charset=utf-8');
        xhrUsers.setRequestHeader('Accept-Language', 'RU');
        xhrUsers.setRequestHeader('Authorization', token);

        xhrUsers.send();

        xhrUsers.onloadend = () => {
            const arr = JSON.parse(xhrUsers.response);
            const res = [...arr.data].map(item => { return item.email; });
            console.log(res);
        };

        xhrUsers.onreadystatechange = () => {
            console.log(xhrUsers.status);
        };
    }
}